# PhinmaNewsWebapp 

PHINMANews is an application that is specifically designed for the Phinma – University of
Pangasinan students. It aims to help the students to stay connected to the university. This
application mainly focuses on providing students with a platform which they can cope with the
current announcement, events, news, and happenings in Phinma – Upang university and they
can have a social engagement at the same time. The app will serve as a digital community for
the students, that includes a feature where they can share their thoughts and ideas on the
comment section of an announcement, news, or post.
